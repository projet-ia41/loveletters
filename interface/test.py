from tkinter import *
import threading, queue

fenetre = Tk()
canvas = Canvas(fenetre,width=350, height=200)
q=queue.Queue()


class Thread1(threading.Thread):
    def __init__(self,q):
        threading.Thread.__init__(self)
        self.q=q
    def run(self):
        img=self.q.get()
        canvas.create_image(0, 0, anchor=NW, image=img)
        canvas.pack()
        fenetre.update()

t=Thread1(q)
#t.start()

imgs={
    "Guard":PhotoImage(file="interface/cards/guard.png"),
    "Priest":PhotoImage(file="interface/cards/priest.png"),
    "Baron":PhotoImage(file="interface/cards/baron.png"),
    "Handmaid":PhotoImage(file="interface/cards/handmaid.png"),
    "Prince":PhotoImage(file="interface/cards/prince.png"),
    "King":PhotoImage(file="interface/cards/king.png"),
    "Countess":PhotoImage(file="interface/cards/countess.png"),
    "Princess":PhotoImage(file="interface/cards/princess.png"),
    "back":PhotoImage(file="interface/cards/back.png")
}

def test():
    canvas.create_image(0, 0, anchor=NW, image=imgs["Guard"])
    fenetre.update()

canvas.create_image(0, 0, anchor=NW, image=imgs["Guard"])
canvas.create_image(50, 0, anchor=NW, image=imgs["Guard"])
canvas.pack()
names=[]
names.append(StringVar()) 
Label(fenetre, text="Name Player 1 :").pack()
Entry(fenetre, textvariable=names[0], width=50).pack()
button = Button(fenetre,text="Test", command=test)
button.pack(side=LEFT, padx=10)

fenetre.mainloop()