# CONTENT
UTBM 2020 IA41 project. Implementation of an IA for the love Letter's game using Q-Learning. You can launch the project on the different branch using the main.py file and reset the different tables using reinitialize.py with the different initialization functions of the class DBAccess in each QLearningEngine package.

# BRANCH MASTER
Up to date with the implementation_interface branch wich allows to play with an interface against the version 1 and 3 of the AI.

# BRANCH test_AI
Allows to play against AI in a console or make AI play against each other. But this branch is mainly use to test AI version 1 against version 3 because prints are disable in console. Results of the games played are store in the db file of the QLearning version 3 in the statistics table.

# BRANCH QLearningV1
Use to train the version 1 of the QLearning AI.

# BRANCH QLearningV3
Use to train the version 3 of the QLearning AI.

# IMPLEMENTATION BRANCH 
Different branch where we first implement our different engines, not meant to be used.

# AUTHORS
Théo GARRIDO
Owein DOURNEAU
Tommy RAMOS
Clément HASLE
