from .playerState import PlayerState, State
from .probability import drawProbability

class GameState(State):

    """Represents a state of the game.

    This class also includes functions to create and return childs from a gameState to simulate the 
    different gameStates corresponding to the nodes of the minMax tree.

    """

    def __init__(self, game, draw=None, discard=None, opponent_state=None, AI_state=None, cumulative_proba=None):
        """Constructor. Can be used with a game instance for the first node of the minmax tree or 
        with variables to instanciate child.

        This class is only meant to be instanciate with a game instance and the opponent
        hand if AI knows it in AI module. And instanciation with paremeters by default with
        None is used in childState to create and return a child state. In this case we put 
        None for the game parameter.
        
        """
        if game:
            self.__discard = self._cardsToIntegers(game.getDiscard())
            self.__AI_state = PlayerState(game.getPlayers()[0])
            self.__opponent_state = PlayerState(game.getPlayers()[1], True)
            self.__draw = self.__createDraw()
            self.__cumulative_proba = 1
            self.__is_leaf = False
            self.__special = None
        else:
            self.__draw = draw
            self.__discard = discard
            self.__AI_state = AI_state
            self.__opponent_state = opponent_state
            self.__is_leaf = self.__isRoundEnded(AI_state.getIsKnockedOut(), opponent_state.getIsKnockedOut(), draw)
            self.__cumulative_proba = cumulative_proba
            self.__special = None
            if self.__is_leaf:
                self.__updateProbaScore()

    def getDraw(self):
        return self.__draw

    def getAIState(self):
        return self.__AI_state

    def getOpponentState(self):
        return self.__opponent_state
    
    def getCumulativeProba(self):
        return self.__cumulative_proba
    
    def getIsLeaf(self):
        return self.__is_leaf

    def getSpecial(self):
        return self.__special

    def __createDraw(self): 
        '''Initialzes the draw.
        
        Used in constructor to instanciate the state of the first node of the minmax tree.

        '''
        draw = [5,2,2,2,2,1,1,1]
        index_card = 0
        for card_number in self.__discard:
            draw[index_card] -= card_number
        draw[self.__AI_state.getHand() - 1] -= 1
        return draw

    def __isRoundEnded(self, AI_knocked_out, opponent_knocked_out, draw):
        '''Returns a boolean equal to true if the round is over and false if not.*
        
        Keyword arguments:
        AI_knocked_out -- a true boolean is AI is knocked out.
        opponent_knocked_out -- a true boolean if opponent is knocked. 
        draw -- list of number of each type of card in the draw.

        '''
        if not draw or AI_knocked_out or opponent_knocked_out:
                return True
        return False

    def __updateProbaScore(self):
        '''Updates the cumulative proba and the score according to the winning player.

        Meant to be called in the gameState contructor if the child created is an ending round. 
        According to MinMax logic and since we use this cumulative probability as the node's 
        utility, if the opponent wins we substract 1 to the cumulative probability to make it negativ. 
        So it corresponds to one of the min mooves to choose from in the tree.
        When AI wins we don't change the probability value and minMax will choose the higher cumulative probability.

        '''
        # if AI looses
            # AI is knocked out
        if ((self.__AI_state.getIsKnockedOut()) or 
            # Draw empty and opponent has a better card
            (not self.__draw and self.__opponent_state.getHand() > self.__AI_state.getHand) or
            # In case of equality and opponent has a better discard
            (not self.__draw and self.__opponent_state.getHand() == self.__AI_state.getHand() and 
            self.__opponent_state.discardValue() >= self.__AI_state.discardValue())):
            self.__cumulative_proba -= 1
            self.__opponent_state.incrementScore()
        else:
            self.__AI_state.incrementScore()

    def childState(self, AI_turn, card_drawn, action, opponent_card_known=False, opponent_card=None):
        '''Returns a child state of the calling gameState considering the situation.

        It also handles the first level of the minmanx tree. What makes this situation special is
        that we don't know oponent's hand since it's not simulated yet. So in this situation opponent_card_known
        indicates if we know his card (throug priest for example) and opponent_card is the value of that card.
        In case we know the card, we know it is not simulate so we don't considerate the chance that he has it.

        Keyword arguments:
        AI_turn -- a true boolean if the AI played the turn
        card_drawn -- card drawn at the begining of the turn
        action -- list which describe the action of the turn, takes different forms according to the card played :
                Default = [card_played]
                Guard = [cardPlayed, targeted_card]
                Prince = [cardPlayed, is_AI_targeted, card_drawn][cardPlayed]
        opponent_card_known -- a true boolean if we know the opponent's hand at the start of the simulation.
        opponent_card -- the value of the opponent's card value (simulated or known)

        '''
        # Set immunity
        AI_immune = self.__setImmuneChild(AI_turn,action,self.__AI_state)
        # Set hands and discards
        opponent_immune = self.__setImmuneChild(AI_turn,action,self.__opponent_state)
        AI_hand = self.__setChildHand(AI_turn, card_drawn, action, self.__AI_state, opponent_card)
        AI_discard = self.__setDiscardChildPlayer(AI_turn, action, self.__AI_state)
        opponent_hand = self.__setChildHand(AI_turn, card_drawn, action, self.__opponent_state, opponent_card)
        opponent_discard = self.__setDiscardChildPlayer(AI_turn, action, self.__opponent_state)
        # Swap hand in case of King played
        if (AI_turn and opponent_immune == False) or (not AI_turn and AI_immune == False):
            if( action[0] == 6):
                AI_hand, opponent_hand = opponent_hand , AI_hand
        #set knocked_out checking immunity
        if AI_immune:
            AI_knocked_out = False
        else:
            AI_knocked_out = self.__setChildKnockedOut(AI_turn, action, self.__AI_state, AI_hand, opponent_hand)
        if opponent_immune:
            opponent_knocked_out = False
        else:
            opponent_knocked_out = self.__setChildKnockedOut(AI_turn, action, self.__opponent_state, AI_hand, opponent_hand)
        # Set dra
        draw, cumulative_proba = self.__setDrawAndProbaChild(card_drawn, action, opponent_card_known, opponent_card)
        discard = self.__setDiscardChild(action)
        # Create child states of opponent and AI state and return new child game state
        child_AI_state = PlayerState(None, False, AI_hand, AI_discard, self.__AI_state.getScore(), AI_immune, AI_knocked_out)
        child_opponent_state = PlayerState(None, False, opponent_hand, opponent_discard, self.__opponent_state.getScore(), opponent_immune, opponent_knocked_out)
        ret = GameState(None, draw, discard, child_opponent_state, child_AI_state, cumulative_proba)
        if(action[0] == 1 or action[0] == 5):
            ret.__special = action[1]
        return ret

    def __setChildHand(self, AI_turn, card_drawn, action, player, opponent_card):
        '''Checks who is playing and return a hand in consequence.

        Keyword arguments:
        AI_turn -- a true boolean if it is the turn of the AI.
        card_drawn -- The card drawn by the player who is playing.
        action -- An array wich handles the card played and the choice made.
        player -- the player to return the value for.
        opponent_card -- opponent card value, only set for the first level of minmax tree.

        '''
        # Opponents initial card, only set for the first level of minmax tree
        if opponent_card and player == self.__opponent_state:
            return opponent_card
        # Prince played.
        if(action[0] == 5):
            # Player to set is targeted
            if((player == self.__AI_state and action[1]) or 
                (player == self.__opponent_state and not action[1])):
                return action[2]
        # Card drawn by AI is played
        if AI_turn and player == self.__AI_state and card_drawn == action[0]:
            return self.__AI_state.getHand()
        # Card drawn by oppponent is played
        elif not AI_turn and player == self.__opponent_state and card_drawn == action[0]:
            return self.__opponent_state.getHand()
        # Player to set is not the one playing
        elif (AI_turn and player == self.__opponent_state) or (not AI_turn and player == self.__AI_state):
            return player.getHand()
        # Card drawn isn't played
        return card_drawn

    def __setDiscardChildPlayer(self, AI_turn, action, player):
        '''Returns a discard for a player considering turn's actions.
        
        Keyword arguments:
        AI_turn -- a true boolean if it is the turn of the AI.
        action -- an array wich handles the card played and the choice made.
        player -- the player to return the value for.
        
        '''
        discard = player.getDiscard().copy()
        # Player to set is the one playing 
        if (AI_turn and player == self.__AI_state) or (not AI_turn and player == self.__opponent_state):
            discard[action[0] - 1] += 1
        # Card played is a Prince and the player to set get targeted.
        if (action[0] == 5) and ((action[1] and player == self.__AI_state) or
            (not action[1] and player == self.__opponent_state)):
            discard[action[2] - 1] += 1
        # Otherwise discard doesn't change
        return discard

    def __setChildKnockedOut(self, AI_turn, action, player, AI_hand, opponent_hand):
        '''Returns knocked out status for a player of a child state considering turn's actions.

        Call setKnockedOutBaron and setKnockedOutGuard in case of these cards were played.
        
        Keyword arguments:
        AI_turn -- boolean which indicate if the AI turn.
        action -- An array wich handle the card played and the choice made.
        player -- the player to return the value for.
        AI_hand -- value of the card in AI's hand.
        opponent_hand -- value of the card in opponent's hand.

        '''
        # Guard
        if action[0] == 1: 
            return self.__setChildKnockedOutGuard(AI_turn, action, player, AI_hand, opponent_hand)
        # Baron
        if action[0] == 3:
            return self.__setChildKnockedOutBaron(player, AI_hand, opponent_hand)
        # Discard princess
        if ((AI_turn and player == self.__AI_state and action[0] == 8) or #AI discards princess
            (not AI_turn and player == self.__opponent_state and action[0] == 8)): #Opponent discards princess
            return True
        # AI discards princess with prince
        if (action[0] == 5 and action[2] == 8 and player == self.__AI_state and action[2]): 
            return True
        # Opponent discards princess with prince
        if (action[0] == 5 and action[2] == 8 and player == self.__opponent_state and not action[2]):
            return True
        return False

    def __setChildKnockedOutBaron(self, player, AI_hand, opponent_hand):
        '''Returns knocked out status for a player of a child state in case of Baron as card played.
        
        Keyword arguments:
        AI_turn -- boolean which indicate if the AI turn.
        action -- An array wich handle the card played and the choice made.
        player -- the player to return the value for.
        AI_hand -- value of the card in AI's hand.
        opponent_hand -- value of the card in opponent's hand.

        '''
            #Set for AI and AI loses
        if ((player == self.__AI_state and AI_hand < opponent_hand) or
            #Set for opponent and opponent loses
            (player == self.__opponent_state and opponent_hand < AI_hand)):
            return True
        return False

    def __setChildKnockedOutGuard(self, AI_turn, action, player, AI_hand, opponent_hand):
        '''Returns knocked out status for a player of a child state in case of Guard as card played.
        
        Keyword arguments:
        AI_turn -- boolean which indicate if the AI turn.
        action -- An array wich handle the card played and the choice made.
        player -- the player to return the value for.
        AI_hand -- value of the card in AI's hand.
        opponent_hand -- value of the card in opponent's hand.

        '''
        # Player to set played and wins
        if ((AI_turn and player == self.__opponent_state and opponent_hand == action[1]) or
            (not AI_turn and player == self.__AI_state and AI_hand == action[1])):
            return True 
        return False
    
    def __setImmuneChild(self, AI_turn, action,player):
        '''Returns immunity status for a player of a child state considering turn's actions.
        
        Keyword arguments:
        AI_turn -- boolean which indicate if the AI turn.
        action -- An array wich handle the card played and the choice made.
        player -- the player to return the value for.

        '''
        # Player to set did not played Handmaid
        if(action[0] != 4) and ((AI_turn and player == self.__AI_state) or (not AI_turn and player == self.__opponent_state)): 
            return False
        # Player to set played Handmaid
        elif( ( AI_turn and player == self.__AI_state ) or (not AI_turn and player == self.__opponent_state) ): 
            return True
        # Player to set is not playing
        if( ( AI_turn and player == self.__opponent_state ) or ( not AI_turn and player == self.__AI_state )):
            return player.getIsImmune()
       
    def __setDrawAndProbaChild(self, card_drawn, action, opponent_card_known, opponent_card):
        '''Returns draw and probability for a child state considering turn's action.

        Opponent_card_known and opponent_card are used for the state of the first level
        of minmax tree when we haven't simulates opponent's hand already.
        
        Keyword arguments:
        card_drawn -- value of the card drawn.
        action -- an array wich handle the card played and the choice made.
        opponent_card_known -- a true boolean if we know opponent's hand.
        opponent_card -- value of opponent's card in case of a state of a first level node in minmax tree.

        '''
        # Copy mother's discard
        draw = self.__draw.copy()
        # Update cumulative probability with the chance of drawing the first card (opponent's 
        # one in case of a first level child or current's player one)
        probability = self.__cumulative_proba
        if opponent_card:
            if opponent_card_known:
                probability *= drawProbability(draw, opponent_card)
            draw[opponent_card-1] -= 1
        else: 
            probability *= drawProbability(draw, card_drawn)
        draw[card_drawn-1] -= 1
        #  Prince is played, same process with second the card drawn
        if(action[0] == 5 ):
            probability *= drawProbability(draw, action[2])
            draw[action[2]-1] -= 1

        return draw, probability
        
    def __setDiscardChild(self, action):
        '''Returns the discard for a child state considering turn's action.
        
        Keyword arguments:
        action -- an array wich handle the card played and the choice made.

        '''
        # Copy mother's dicard and update
        discard = self.__discard.copy()
        discard[action[0]-1] += 1 
        # Prince is played
        if(action[0] == 5):
            discard[action[2]-1] += 1
        return discard