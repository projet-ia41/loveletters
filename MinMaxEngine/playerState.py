from .state import State

class PlayerState(State):

    """Represents a state of a player."""

    def __init__(self, player, is_opponent=False, hand=None, discard=None, score=None, is_immune=None, is_knocked_out=None):
        """Constructor, can be used with a player instance for the first node of the minmax tree or 
        with variables to instanciate child.
        
        """
        # Used to create a state from a player object (first level states in the minMax tree).
        if player:
            if not is_opponent:
                self.__hand = player.getHand()[0].getValue()
            else:
                # We don't know opponent's hand for the first turn
                self.__hand = None
            self.__discard = self._cardsToIntegers(player.getDiscard())
            self.__score = player.getScore()
            self.__is_immune = False
            self.__is_knocked_out = False
        # Used to create childStates from a player state in childState() function in the class gameState.
        else:
            self.__hand = hand
            self.__discard = discard
            self.__score = score
            self.__is_immune = is_immune
            self.__is_knocked_out = is_knocked_out

    def getHand(self):
        return self.__hand
        
    def getDiscard(self):
        return self.__discard
    def setDiscard(self, discard):
        """Only implemented for test purpose."""
        self.__discard = discard

    def getScore(self):
        return self.__score

    def getIsKnockedOut(self):
        return self.__is_knocked_out
    
    def getIsImmune(self):
        return self.__is_immune
    
    def incrementScore(self):
        """Increment player's score."""
        self.__score += 1

    def discardValue(self):
        """Calculate and return the total value of the discard of the player."""
        sum = 0
        value_card = 0
        for card_number in self.__discard:
            value_card += 1
            sum += card_number * value_card
        return sum