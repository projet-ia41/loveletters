
def numberOfCards(draw):
    '''Return the number of cards in the current draw.
     Parameters : 
     - draw : a given draw
    '''
    nbCards = 0
    for card in draw:
        nbCards += card

    return nbCards


def drawProbability(draw,card_drawn):
    '''Return the probability to draw the card.
    Parameters : 
     - draw : a given draw
     - card_drawn : the card drawed
    '''
    nbCards = numberOfCards(draw)
    return draw[card_drawn - 1] / nbCards 


