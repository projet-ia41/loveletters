from abc import ABC 

CARD_INDEX_MATCHING = {
    0 : "Guard",
    1 : "Priest",
    2 : "Baron",
    3 : "Handmaid",
    4 : "Prince",
    5 : "King",
    6 : "Countess",
    7 : "Princess"
}

class State(ABC):

    @staticmethod
    def _cardsToIntegers(card_list):
        """Transform a cards list into an ordered integer list corresponding of
        the number of each card in the list.

        IN:
        card_list -- list of cards
        OUT:
        List of integer corresponding to the list of cards
            Look like this:
            [number of guards, number of priest, ..., number of princess]

        """
        integer_list = [0, 0, 0, 0, 0, 0, 0, 0]
        for card in card_list:
            for index, card_name in CARD_INDEX_MATCHING.items():
                if card.getName() == card_name:
                    integer_list[index] += 1
        return integer_list