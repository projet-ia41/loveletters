import unittest
from gameEngine.player import Player
from AIEngine.AI import AI
from AIEngine.playerState import PlayerState
from gameEngine.guard import Guard
from gameEngine.priest import Priest
from gameEngine.baron import Baron
from gameEngine.handmaid import Handmaid
from gameEngine.prince import Prince
from gameEngine.king import King
from gameEngine.countess import Countess
from gameEngine.princess import Princess

class TestPlayerState(unittest.TestCase):

    def setUp(self):
        self.draw = [Guard(), Baron(), Prince(), Guard(), Countess()]
        self.AI_player = AI(0)
        self.opponent = Player(1, "test")
        self.AI_player.draw(self.draw)
        self.opponent.draw(self.draw)
        self.AI_player.draw(self.draw)
        self.AI_state = PlayerState(self.AI_player)
        self.opponent_state = PlayerState(self.opponent, True)
        # AI hand should be [Guard, Prince] and opponent's [Baron]
        # draw should be [Guard, Countess]

    def testInitialisation(self):
        """Verify the state of the different attributes of AI_state and opponent state
        after first instanciation.
        
        """
        self.assertEqual(self.AI_state.getDiscard(), [0,0,0,0,0,0,0,0])
        self.assertEqual(self.opponent_state.getDiscard(), [0,0,0,0,0,0,0,0])
        self.assertEqual(self.AI_state.getHand(), 1)
        self.assertEqual(self.opponent_state.getHand(), None)
        self.assertEqual(self.AI_state.getIsImmune(), False)
        self.assertEqual(self.opponent_state.getIsImmune(), False)
        self.assertEqual(self.AI_state.getIsKnockedOut(), False)
        self.assertEqual(self.opponent_state.getIsKnockedOut(), False)
        self.assertEqual(self.AI_state.getScore(), 0)
        self.assertEqual(self.opponent_state.getScore(), 0)

    def testDiscardValue(self):
        self.assertEqual(self.AI_state.discardValue(), 0)
        self.AI_state.setDiscard([2,1,0,1,0,1,0,0])
        self.assertEqual(self.AI_state.discardValue(), 14)
        self.AI_state.setDiscard([0,1,0,1,0,0,0,1])
        self.assertEqual(self.AI_state.discardValue(), 14)
        self.AI_state.setDiscard([0,0,0,1,0,0,0,0])
        self.assertEqual(self.AI_state.discardValue(), 4)
        
        self.assertEqual(self.opponent_state.discardValue(), 0)
        self.opponent_state.setDiscard([2,1,0,1,0,1,0,0])
        self.assertEqual(self.opponent_state.discardValue(), 14)
        self.opponent_state.setDiscard([0,1,0,1,0,0,0,1])
        self.assertEqual(self.opponent_state.discardValue(), 14)
        self.opponent_state.setDiscard([0,0,0,1,0,0,0,0])
        self.assertEqual(self.AI_state.discardValue(), 4)

    def testIncrementScore(self):
        self.assertEqual(self.AI_state.getScore(), 0)
        self.AI_state.incrementScore()
        self.assertEqual(self.AI_state.getScore(), 1)
        self.AI_state.incrementScore()
        self.AI_state.incrementScore()
        self.assertEqual(self.AI_state.getScore(), 3)
        
        self.assertEqual(self.opponent_state.getScore(), 0)
        self.opponent_state.incrementScore()
        self.assertEqual(self.opponent_state.getScore(), 1)
        self.opponent_state.incrementScore()
        self.opponent_state.incrementScore()
        self.assertEqual(self.opponent_state.getScore(), 3)