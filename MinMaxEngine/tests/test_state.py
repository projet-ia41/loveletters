import unittest
from AIEngine.state import State
from gameEngine.guard import Guard
from gameEngine.priest import Priest
from gameEngine.baron import Baron
from gameEngine.handmaid import Handmaid
from gameEngine.prince import Prince
from gameEngine.king import King
from gameEngine.countess import Countess
from gameEngine.princess import Princess

class TestState(unittest.TestCase):
    
    __test_draw = [Prince(), Guard(), Handmaid(), Guard(), Baron(), Guard(), Priest(), Guard(), Countess(), Baron()]

    def setUp(self):
        pass

    def testCardsToIntegers(self):
        self.assertEqual(State._cardsToIntegers([]), [0,0,0,0,0,0,0,0])
        self.assertEqual(State._cardsToIntegers(self.__test_draw), [4,1,2,1,1,0,1,0])
        self.__test_draw.pop(1)
        self.__test_draw.pop(1)
        self.assertEqual(State._cardsToIntegers(self.__test_draw), [3,1,2,0,1,0,1,0])

    def tearDown(self):
        pass