import math
from gameEngine.player import Player
from .gameState import GameState

id = 1

class AI(Player):

    def __init__(self,id):
        super().__init__(id, "IA")
        self.__memorized_opponent_card = None
        self.__special = None

    def playCard(self, game):
        super().playCard(game)
    
    def draw(self, draw):
        super().draw(draw)
    
    def discardCard(self, draw, players):
        super().discardCard(draw, players)

    def calcTie(self):
        super().calcTie()

    def setMemorizedOpponentCard(self, card):
        self.__memorized_opponent_card = card

    def __forgetOpponentCard(self, opponent_discard):
        """Check if memorized card was played.

        opponent_discard = opponent discard

        """
        nb_card_to_check = 1
        len_opponent_discard =len(opponent_discard)
        if len_opponent_discard == len(self._discard)-2:
            nb_card_to_check = 2
        while nb_card_to_check != 0 and self.__memorized_opponent_card != None:
            self.__memorized_opponent_card = None if opponent_discard[len_opponent_discard-nb_card_to_check] == self.__memorized_opponent_card else self.__memorized_opponent_card
            nb_card_to_check-=1

    def getSpecial(self):
        return self.__special

    def _chooseCard(self, game):
        """Choose a card to play thanks to minimax.

        game = game object

        Return : index of card to play

        """
        opponent = None
        if game.getPlayers()[0] == self:
            self.__forgetOpponentCard(game.getPlayers()[1].getDiscard())
            opponent = game.getPlayers()[1]
        else:
            self.__forgetOpponentCard(game.getPlayers()[0].getDiscard())
        choosen_card = self.__instaWin()
        opponent = game.getPlayers()[0]
        if choosen_card != -1 and not opponent.getIsImmune():
            return choosen_card
        state = GameState(game)
        next_states = self.__firstNextStates(state)
        print(next_states)
        best_utility = math.inf
        choosen_state = None
        for next_state in next_states:
            utility = self.__minimax(next_state,float(-math.inf), float(math.inf),False)
            if best_utility < utility or best_utility == math.inf:
                best_utility = utility
                choosen_card = 1 if next_state.getAIState().getDiscard()[0] != self._hand[0].getValue() else 0
                choosen_state = next_state
        if(self._hand[choosen_card].getValue() == 1 or self._hand[choosen_card].getValue() == 5):
            print(best_utility)
            print(choosen_card)
            print(choosen_state)
            self.__special = choosen_state.getSpecial()
        return choosen_card

    def __minimax(self,state,alpha,beta,max_player,nbRecur=1):
        """Minimax algorithm.

        state = actual state
        alpha = lowest value
        beta = bigger value
        max_player = bool, if AI is playing or not

        return NodeEval or utility if leaf

        """
        #print("Id : {} ParentId : {} Étage : {}".format(state.id, state.parentId, nbRecur))
        if state.getIsLeaf() or nbRecur == 5:
            return state.getCumulativeProba()

        childs=self.__nextStates(state, max_player)
        i=0
        if max_player:
            maxEval =float(-math.inf)
            while beta > alpha and i<len(childs):
                evalC = self.__minimax(childs[i], alpha, beta, False,nbRecur+1)
                maxEval=max(maxEval,evalC)
                alpha=max(alpha,evalC)
                i+=1
            return maxEval
        else:
            minEval=float(math.inf)
            while beta > alpha and i<len(childs):
                evalC = self.__minimax(childs[i], alpha, beta, True,nbRecur+1)
                minEval=min(minEval,evalC)
                beta=min(beta,evalC)
                i+=1
            return minEval

    def __utility(self, state):
        pass

    def __firstNextStates(self, state):
        """Generate first next states from state game

        state = game state

        return = list of first next state
        """
        nextStates = []
        global id
        if self.__memorized_opponent_card != None :
            for card_hand in self._hand:
                actions = self.__cardActions(card_hand.getValue(), state.getDraw(), state.getOpponentState(),self._hand[1].getValue())
                for action in actions:
                    nextStates.append(state.childState(True, self._hand[1].getValue(), action, True, self.__memorized_opponent_card))
        else :
            for card_hand in self._hand:
                actions = self.__cardActions(card_hand.getValue(), state.getDraw(), state.getOpponentState(),self._hand[1].getValue())
                for action in actions:
                    for card_drawn in state.getDraw():
                        card_drawn_value=1
                        if card_drawn != 0:
                            nextStates.append(state.childState(True, self._hand[1].getValue(), action, False, card_drawn_value))
                        card_drawn_value+=1
        nextStates.sort(key=lambda next_state: -next_state.getCumulativeProba())
        return nextStates

    def __nextStates(self, state, max_player_turn):
        """Generate next states from mother state.

        state = mother state
        max_player_turn = boolean, if True AI is playing, if not opponent is

        return = list of next state

        """
        draw = state.getDraw()
        current_player_state = state.getAIState() if max_player_turn else state.getOpponentState()
        opponent_state = state.getOpponentState() if max_player_turn else state.getAIState()
        card_drawn_value=1
        nextStates=[]
        global id
        for card_drawn in draw:
            if card_drawn != 0:
                temp_hand=[current_player_state.getHand(), card_drawn_value]
                for card_hand_value in temp_hand:
                    actions = self.__cardActions(card_hand_value, draw, opponent_state, card_drawn_value)
                    for action in actions:
                        nextStates.append(state.childState(max_player_turn, card_drawn_value, action))
                        id+=1
            card_drawn_value+=1
        max_player_turn if nextStates.sort(key=lambda next_state: next_state.getCumulativeProba()) else nextStates.sort(key=lambda next_state: -next_state.getCumulativeProba())
        return nextStates

    def __cardActions(self, card_value, draw, opponent_state, card_drawn):
        """Choose card actions

        card_value = card value
        draw = list of the number of each type of card in the draw
        opponent_state = state of opponent

        return = list of actions
        """
        actions = []
        if card_value == 1:
            actions=self.__playGuard(draw, opponent_state, card_drawn)
        elif card_value == 5:
            actions=self.__playPrince(draw, card_drawn)
        else: #2-3-4-6-7
            actions=[[card_value]]
        return actions

    def __playGuard(self, draw, opponent_state, card_drawn):
        """Actions when a guard was played.

        draw = list of the number of each type of card in the draw
        opponent_state = opponent state

        return = list of action

        """
        opponent_card_value = 1
        actions=[]
        for opponent_card in draw:
            if opponent_card_value != 1 and opponent_card != 0 and draw[card_drawn-1]-1 != 0:
                actions.append([1, opponent_card_value])
            opponent_card_value+=1
        if(opponent_state.getHand() != None):
            actions.append([1, opponent_state.getHand()])
        return actions

    def __playPrince(self, draw, card_drawn):
        """Actions when a prince was played.

        draw = list of the number of each type of card in the draw

        return = list of action

        """
        players=[True, False]
        actions=[]
        for player in players:
            card_in_draw_value = 1
            for card_in_draw in draw:
                if card_in_draw != 0 and draw[card_drawn-1]-1 != 0:
                    actions.append([5, player, card_in_draw_value])
                card_in_draw_value+=1
        return actions

    def __instaWin(self):
        """Check if a the A.I can instant win

        return = card choose if A.I can else -1
        """
        if self.__memorized_opponent_card != None :
            have_guard = False
            have_baron = False
            have_prince = False
            have_princess = False
            for card in self._hand:
                have_guard = False if not have_guard and card.getValue() != 1 else True
                have_baron = False if not have_baron and card.getValue() != 3 else True
                have_prince = False if not have_prince and card.getValue() != 5 else True
                have_princess = False if not have_princess and card.getValue() != 8 else True
            
            if have_guard :
                return 0 if self._hand[0].getValue() == 1 else 1
            if have_baron and (self.__memorized_opponent_card < 3 or have_princess):
                return 0 if self._hand[0].getValue() == 3 else 1
            if have_prince and self.__memorized_opponent_card == 8:
                return 0 if self._hand[0].getValue() == 1 else 1
        return -1